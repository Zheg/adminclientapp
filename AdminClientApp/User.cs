﻿using System;
using Newtonsoft.Json;

namespace AdminClientApp
{
    public class User
    {
        public readonly string Name;
        public readonly int[] Password; // not byte[] to easy save in JSON into txt
        public readonly bool IsBlocked;
        public readonly bool HasPasswordRestriction;

        [JsonConstructorAttribute]
        public User(string name, int[] password, bool isBlocked, bool hasPasswordRestriction)
        {
            Name = name;
            Password = password ?? new int[0];
            IsBlocked = isBlocked;
            HasPasswordRestriction = hasPasswordRestriction;
        }
        
        public User(string name, string password, bool isBlocked, bool hasPasswordRestriction)
        {
            Name = name;
            Password = PasswordManager.Encrypt(password);
            IsBlocked = isBlocked;
            HasPasswordRestriction = hasPasswordRestriction;
        }

        public static User CreateEmpty(string name)
        {
            return new User(name, "", false, true);
        }

        public UserWithoutPassword CopyWithoutPassword()
        {
            return new UserWithoutPassword(Name, IsBlocked, HasPasswordRestriction);
        }

        public static User Merge(User user, UserWithoutPassword userWithoutPassword)
        {
            return new User(
                userWithoutPassword.Name,
                user.Password,
                userWithoutPassword.IsBlocked,
                userWithoutPassword.HasPasswordRestriction);
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static User FromJson(string json)
        {
            var user = JsonConvert.DeserializeObject<User>(json);
            if (user == null) throw new Exception("Data is corrupted.");

            return user;
        }

        public int[] GetPassword()
        {
            return Password;
        }
    }
}