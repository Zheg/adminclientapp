﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace AdminClientApp
{
    public partial class Client : Form
    {
        private readonly UserWithoutPassword _user;

        private readonly UserRepository _userRepository;

        private LogIn _logInForm;

        private List<UserWithoutPassword> _users;

        private int _index = 0;

        public bool PasswordChanged = true;

        public Client(UserWithoutPassword user, UserRepository userRepository, bool needToChangePassword,
            LogIn logInForm)
        {
            _user = user;
            _userRepository = userRepository;
            _logInForm = logInForm;
            InitializeComponent();

            _user = _userRepository.GetUser(_user.Name);
            var isAdmin = _user.Name == "ADMIN";
            UserPanel.Visible = !isAdmin;
            AdminPanel.Visible = isAdmin;
            addUserToolStripMenuItem.Visible = isAdmin;
            
            if (isAdmin)
            {
                _users = _userRepository.GetAllUsers();
                BeginButton_Click(this, EventArgs.Empty);
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form about = new About();
            about.ShowDialog();
        }

        public void ChangePasswordAction()
        {
            Form changePassword = new ChangePassword(_user, _userRepository, this);
            changePassword.ShowDialog();
        }

        private void ChangePassword_Click(object sender, EventArgs e)
        {
            ChangePasswordAction();
        }

        private void addUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form addUser = new AddUser(_userRepository);
            addUser.ShowDialog();
            _users = _userRepository.GetAllUsers();
            UpdateView();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (_logInForm.Visible == false)
                _logInForm.Close();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BeginButton_Click(object sender, EventArgs e)
        {
            _index = 0;
            UpdateView();
        }

        private void UpdateView()
        {
            Order.Text = _index + 1 + "/" + _users.Count;

            var user = _users[_index];

            LoginOutput.Text = user.Name;

            IsBlockedCheckBox.Checked = user.IsBlocked;
            IsBlockedCheckBox.Enabled = user.Name != "ADMIN";
            DeleteButton.Enabled = user.Name != "ADMIN";

            PasswordRestrictionCheckBox.Checked = user.HasPasswordRestriction;

            PrevButton.Enabled = _index != 0;
            NextButton.Enabled = _index != _users.Count - 1;
        }

        private void EndButton_Click(object sender, EventArgs e)
        {
            _index = _users.Count - 1;
            UpdateView();
        }

        private void PrevButton_Click(object sender, EventArgs e)
        {
            --_index;
            UpdateView();
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            ++_index;
            UpdateView();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            _userRepository.Update(new UserWithoutPassword(
                LoginOutput.Text,
                IsBlockedCheckBox.Checked,
                PasswordRestrictionCheckBox.Checked));

            _users = _userRepository.GetAllUsers();
            MessageBox.Show("User \"" + LoginOutput.Text + "\" saved.", "User saved", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            BeginButton_Click(sender, e);
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            var confirmation = MessageBox.Show("Are you sure want to delete \"" + LoginOutput.Text + "\"?", "Delete user",
                MessageBoxButtons.YesNo);
            if (confirmation == DialogResult.Yes)
            {
                _userRepository.Delete(LoginOutput.Text);
                _users = _userRepository.GetAllUsers();
                MessageBox.Show("User \"" + LoginOutput.Text + "\" deleted.", "User deleted", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                BeginButton_Click(sender, e);
            }
        }
    }
}