﻿using System;
using System.Windows.Forms;

namespace AdminClientApp
{
    public partial class LogIn : Form
    {
        private int _wrongInputCount = 0;
        private readonly UserRepository _userRepository;
        private bool _passphraseProvided = false;

        public LogIn()
        {
            InitializeComponent();
            _userRepository = new UserRepository();
            if (_userRepository.NewFile)
            {
                var createNewPassPhrase = new CreateNewPassPhrase(this);
                createNewPassPhrase.ShowDialog();
            }
            else
            {
                var inputPassphrase = new InputPassphrase(this);
                inputPassphrase.ShowDialog();
            }

            if (!_passphraseProvided)
            {
                Close();
                return;
            }
            
            if (!_userRepository.HasAdmin())
            {
                MessageBox.Show("Wrong passphrase inputted.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form about = new About();
            about.ShowDialog();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            var login = LoginInput.Text;
            var password = PasswordInput.Text;

            if (_userRepository.Auth(login, password))
            {
                var user = _userRepository.GetUser(login);
                if (user.IsBlocked)
                {
                    MessageBox.Show("User is blocked!", "Log in error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Client client = new Client(user, _userRepository, PasswordInput.Text == "", this);
                Hide();
                client.Show();
                if (PasswordInput.Text == "")
                {
                    
                    MessageBox.Show("After first log in You should change password.", "First log in", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    client.PasswordChanged = false;
                    client.ChangePasswordAction();
                }
                else if (user.HasPasswordRestriction && !PasswordManager.HasRestrictions(PasswordInput.Text))
                {
                    MessageBox.Show("ADMIN enable restriction rules to password for your account. You should change password.", "Log in", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    
                    client.PasswordChanged = false;
                    client.ChangePasswordAction();
                }

                if (!client.PasswordChanged)
                {
                    Show();
                    client.Close();
                }

                _wrongInputCount = 0;
                AttemptLabel.Text = "Attempts left: " + (3 - _wrongInputCount);
            }
            else
            {
                _wrongInputCount++;
                if (_wrongInputCount >= 3)
                {
                    MessageBox.Show("Log in tries limit exceeded. Forced exit.", "Log in error", MessageBoxButtons.OK,
                        MessageBoxIcon.Stop);
                    ExitButton_Click(sender, e);
                }

                MessageBox.Show("Wrong login or password.", "Log in error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                PasswordInput.Text = "";
                AttemptLabel.Text = "Attempts left: " + (3 - _wrongInputCount);
            }
        }

        public void ProvideNewPassphrase(string passphrase)
        {
            _userRepository.GenerateKey(passphrase);
            _passphraseProvided = true;
        }

        public void ProvidePassphrase(string passphrase)
        {
            _userRepository.CalculateKey(passphrase);
            _passphraseProvided = true;
        }
        
        private void LogIn_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_passphraseProvided)
                _userRepository.SaveToFile();
        }
    }
}