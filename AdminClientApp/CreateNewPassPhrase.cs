﻿using System;
using System.Windows.Forms;

namespace AdminClientApp
{
    public partial class CreateNewPassPhrase : Form
    {
        private readonly LogIn _parent;
        public CreateNewPassPhrase(LogIn parent)
        {
            InitializeComponent();
            _parent = parent;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (Passphrase.Text != Confirmation.Text)
            {
                MessageBox.Show("Passphrases are not equal.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            _parent.ProvideNewPassphrase(Passphrase.Text);
            Close();
        }
    }
}