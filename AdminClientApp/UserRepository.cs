﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace AdminClientApp
{
    public class UserRepository
    {
        private readonly string _pathToTempFile = Path.GetTempFileName();
        private readonly string _pathToFile = "users.txt";
        private readonly string _pathToSalt = "salt.txt";

        public readonly bool NewFile = false;

        private byte[] _salt;
        private byte[] _key;
        private const int SaltCount = 64;

        public UserRepository()
        {
            if (!File.Exists(_pathToFile))
            {
                NewFile = true;
            }
        }

        public UserRepository(string path)
        {
            _pathToFile = path;
            if (!File.Exists(_pathToFile))
            {
                NewFile = true;
            }
        }

        public void GenerateKey(string passphrase)
        {
            _salt = new byte[SaltCount];
            new RNGCryptoServiceProvider().GetBytes(_salt);
            SaveSalt();
            
            var passphraseBytes = Encoding.UTF32.GetBytes(passphrase);
            var bytes = passphraseBytes.Concat(_salt).ToArray();
            _key = new MD5CryptoServiceProvider().ComputeHash(bytes);
            
            AppendUser(User.CreateEmpty("ADMIN"));
        }

        public void CalculateKey(string passphrase)
        {
            var passphraseBytes = Encoding.UTF32.GetBytes(passphrase);;
            ReadSalt();
            var bytes = passphraseBytes.Concat(_salt).ToArray();
            _key = new MD5CryptoServiceProvider().ComputeHash(bytes);
            
            Decrypt();
        }

        private void AppendUser(User user)
        {
            using (var sw = GetStreamWriter(_pathToTempFile))
            {
                var json = user.ToJson();
                sw.WriteLine(json);
            }
        }

        public bool HasUser(string userName)
        {
            using (var sr = GetStreamReader(_pathToTempFile))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    var user = User.FromJson(line);
                    if (user.Name == userName) return true;
                }
            }

            return false;
        }

        public void Create(User user)
        {
            if (HasUser(user.Name))
                throw new UserAlreadyExistsException("User with name \"" + user.Name + "\"  already exists.");

            AppendUser(user);
        }

        private User GetUserWithPassword(string userName)
        {
            using (var sr = GetStreamReader(_pathToTempFile))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    var user = User.FromJson(line);
                    if (user.Name == userName) return user;
                }
            }

            throw new UserDoesNotExistsException("User with name \"" + userName + "\"  does not exists.");
        }

        private User GetUserWithPasswordOrNull(string userName)
        {
            try
            {
                return GetUserWithPassword(userName);
            }
            catch (UserDoesNotExistsException userDoesNotExistsException)
            {
                return null;
            }
            catch
            {
                throw;
            }
        }

        public UserWithoutPassword GetUser(string userName)
        {
            return GetUserWithPassword(userName).CopyWithoutPassword();
        }

        public UserWithoutPassword GetUserOrNull(string userName)
        {
            var user = GetUserWithPasswordOrNull(userName);
            return user?.CopyWithoutPassword();
        }

        public bool Auth(string userName, string password)
        {
            var user = GetUserWithPasswordOrNull(userName);
            return user != null && PasswordManager.IsSamePassword(password, user.GetPassword());
        }

        public List<UserWithoutPassword> GetAllUsers()
        {
            var users = new List<UserWithoutPassword>();

            using (var sr = new StreamReader(_pathToTempFile))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    var user = User.FromJson(line);
                    users.Add(user.CopyWithoutPassword());
                }
            }

            return users;
        }

        public void Update(User newUser)
        {
            var tempFile = Path.GetTempFileName();
            using (var sr = GetStreamReader(_pathToTempFile))
            using (var sw = GetStreamWriter(tempFile))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    var user = User.FromJson(line);
                    if (user.Name != newUser.Name)
                        sw.WriteLine(line);
                    else
                        sw.WriteLine(newUser.ToJson());
                }
            }

            File.Delete(_pathToTempFile);
            File.Move(tempFile, _pathToTempFile);
        }

        public void Update(UserWithoutPassword userWithoutPassword)
        {
            var user = GetUserWithPassword(userWithoutPassword.Name);
            user = User.Merge(user, userWithoutPassword);
            Update(user);
        }

        public void Delete(string userName)
        {
            var tempFile = Path.GetTempFileName();
            using (var sr = GetStreamReader(_pathToTempFile))
            using (var sw = GetStreamWriter(tempFile))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    var user = User.FromJson(line);
                    if (user.Name != userName) sw.WriteLine(line);
                }
            }

            File.Delete(_pathToTempFile);
            File.Move(tempFile, _pathToTempFile);
        }

        public void SaveToFile()
        {
            var content = File.ReadAllText(_pathToTempFile);
            var file = File.Open(_pathToFile, FileMode.Create);

            var rmCrypto = new RijndaelManaged();
            rmCrypto.Mode = CipherMode.ECB;
            rmCrypto.Key = _key;

            var encryptor = rmCrypto.CreateEncryptor();
            var crypt = new CryptoStream(file, encryptor, CryptoStreamMode.Write);
           
            using (var sw = new StreamWriter(crypt, Encoding.Unicode))
            {
                sw.Write(content);         
                sw.Flush();
            }
            file.Close();
        }

        private void Decrypt()
        {
            var file = File.Open(_pathToFile, FileMode.Open);

            var rmCrypto = new RijndaelManaged();
            rmCrypto.Mode = CipherMode.ECB;
            rmCrypto.Key = _key;

            var decryptor = rmCrypto.CreateDecryptor();
            var crypt = new CryptoStream(file, decryptor, CryptoStreamMode.Read);

            using (var sr = new StreamReader(crypt, Encoding.Unicode))
            {
                var plaintext = sr.ReadToEnd();
                File.WriteAllText(_pathToTempFile, plaintext);
            }
            file.Close();
        }

        public bool HasAdmin()
        {
            try
            {
                GetUserWithPassword("ADMIN");
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        private void ReadSalt()
        {
            using (var sr = GetStreamReader(_pathToSalt))
            {
                _salt = JsonConvert.DeserializeObject<byte[]>(sr.ReadLine() ?? throw new Exception("No salt provided in file."));
            }
        }
        
        private void SaveSalt()
        {
            using (var sw = new StreamWriter(_pathToSalt, false))
            {
                sw.WriteLine(JsonConvert.SerializeObject(_salt));
            }
        }

        private static StreamWriter GetStreamWriter(string path)
        {
            return new StreamWriter(path, File.Exists(path), Encoding.UTF8);
        }

        private static StreamReader GetStreamReader(string path)
        {
            return new StreamReader(path, Encoding.UTF8);
        }
    }
}