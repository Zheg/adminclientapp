﻿using System.Text;

namespace AdminClientApp
{
    public static class PasswordManager
    {
        public static int[] Encrypt(string password)
        {
            var bytes = Encoding.UTF8.GetBytes(password);
            var ciphertext = SubstitutionEncrypting(bytes);
            return ciphertext;
        }

        private static int[] SubstitutionEncrypting(byte[] bytes)
        {
            var ciphertext = new int[bytes.Length];
            if (bytes.Length == 0)
                return ciphertext;
            for (int i = 0; i < bytes.Length - 1; i++)
            {
                ciphertext[i + 1] = (bytes[i] ^ bytes[i + 1]);
            }

            ciphertext[0] = (bytes[0] ^ ciphertext[bytes.Length - 1]);

            return ciphertext;
        }
        
        private static byte[] SubstitutionDecrypting(int[] ciphertext)
        {
            var bytes = new byte[ciphertext.Length];
            if (ciphertext.Length == 0)
                return bytes;
            bytes[0] = (byte) (ciphertext[0] ^ ciphertext[ciphertext.Length - 1]);
            for (int i = 0; i < bytes.Length - 1; i++)
            {
                bytes[i + 1] = (byte) (bytes[i] ^ ciphertext[i + 1]);
            }

            return bytes;
        }

        private static string Decrypt(int[] ciphertext)
        {
            var bytes = SubstitutionDecrypting(ciphertext);
            return Encoding.UTF8.GetString(bytes);
        }

        public static bool IsSamePassword(string inputPassword, int[] ciphertext)
        {
            var decryptedPassword = Decrypt(ciphertext);
            return decryptedPassword == inputPassword;
        }

        public static bool HasRestrictions(string password)
        {
            var hasDigit = false;
            var hasOperation = false;
            foreach (var symbol in password)
            {
                hasDigit |= char.IsDigit(symbol);
                hasOperation |= IsOperation(symbol);
            }

            return hasDigit && hasOperation;
        }

        private static bool IsOperation(char c)
        {
            return c == '*' || c == '/' || c == '+' || c == '-';
        }
    }
}