﻿using System;

namespace AdminClientApp
{
    public class UserDoesNotExistsException : Exception
    {
        public UserDoesNotExistsException(string message) : base(message)
        {
        }
    }

    public class UserAlreadyExistsException : Exception
    {
        public UserAlreadyExistsException(string message) : base(message)
        {
        }
    }
}