﻿namespace AdminClientApp
{
    public class UserWithoutPassword
    {
        public readonly string Name;
        public readonly bool IsBlocked;
        public readonly bool HasPasswordRestriction;

        public UserWithoutPassword(string name, bool isBlocked, bool hasPasswordRestriction)
        {
            Name = name;
            IsBlocked = isBlocked;
            HasPasswordRestriction = hasPasswordRestriction;
        }

        public User AddPassword(string password)
        {
            return new User(Name, password, IsBlocked, HasPasswordRestriction);
        }
    }
}