﻿using System.ComponentModel;

namespace AdminClientApp
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Client));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.programToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangePasswordButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.UserPanel = new System.Windows.Forms.Panel();
            this.AdminPanel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.Order = new System.Windows.Forms.Label();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.NextButton = new System.Windows.Forms.Button();
            this.PrevButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.PasswordRestrictionCheckBox = new System.Windows.Forms.CheckBox();
            this.IsBlockedCheckBox = new System.Windows.Forms.CheckBox();
            this.LoginOutput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.EndButton = new System.Windows.Forms.Button();
            this.BeginButton = new System.Windows.Forms.Button();
            this.AddUserButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.AdminPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.programToolStripMenuItem, this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(323, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // programToolStripMenuItem
            // 
            this.programToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.changePasswordToolStripMenuItem, this.addUserToolStripMenuItem, this.exitToolStripMenuItem});
            this.programToolStripMenuItem.Name = "programToolStripMenuItem";
            this.programToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.programToolStripMenuItem.Text = "Program";
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.changePasswordToolStripMenuItem.Text = "Change password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.ChangePassword_Click);
            // 
            // addUserToolStripMenuItem
            // 
            this.addUserToolStripMenuItem.Name = "addUserToolStripMenuItem";
            this.addUserToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.addUserToolStripMenuItem.Text = "Add user";
            this.addUserToolStripMenuItem.Click += new System.EventHandler(this.addUserToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // ChangePasswordButton
            // 
            this.ChangePasswordButton.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.ChangePasswordButton.Location = new System.Drawing.Point(12, 27);
            this.ChangePasswordButton.Name = "ChangePasswordButton";
            this.ChangePasswordButton.Size = new System.Drawing.Size(299, 23);
            this.ChangePasswordButton.TabIndex = 1;
            this.ChangePasswordButton.Text = "Change password";
            this.ChangePasswordButton.UseVisualStyleBackColor = true;
            this.ChangePasswordButton.Click += new System.EventHandler(this.ChangePassword_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.ExitButton.Location = new System.Drawing.Point(12, 359);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(299, 23);
            this.ExitButton.TabIndex = 2;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // UserPanel
            // 
            this.UserPanel.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.UserPanel.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("UserPanel.BackgroundImage")));
            this.UserPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.UserPanel.Location = new System.Drawing.Point(12, 58);
            this.UserPanel.Name = "UserPanel";
            this.UserPanel.Size = new System.Drawing.Size(299, 295);
            this.UserPanel.TabIndex = 3;
            this.UserPanel.Tag = "";
            // 
            // AdminPanel
            // 
            this.AdminPanel.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.AdminPanel.Controls.Add(this.label3);
            this.AdminPanel.Controls.Add(this.label2);
            this.AdminPanel.Controls.Add(this.Order);
            this.AdminPanel.Controls.Add(this.DeleteButton);
            this.AdminPanel.Controls.Add(this.NextButton);
            this.AdminPanel.Controls.Add(this.PrevButton);
            this.AdminPanel.Controls.Add(this.SaveButton);
            this.AdminPanel.Controls.Add(this.PasswordRestrictionCheckBox);
            this.AdminPanel.Controls.Add(this.IsBlockedCheckBox);
            this.AdminPanel.Controls.Add(this.LoginOutput);
            this.AdminPanel.Controls.Add(this.label1);
            this.AdminPanel.Controls.Add(this.EndButton);
            this.AdminPanel.Controls.Add(this.BeginButton);
            this.AdminPanel.Controls.Add(this.AddUserButton);
            this.AdminPanel.Location = new System.Drawing.Point(12, 58);
            this.AdminPanel.Name = "AdminPanel";
            this.AdminPanel.Size = new System.Drawing.Size(299, 295);
            this.AdminPanel.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(0, -3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(299, 2);
            this.label2.TabIndex = 5;
            this.label2.Text = "label2";
            // 
            // Order
            // 
            this.Order.Location = new System.Drawing.Point(90, 167);
            this.Order.Name = "Order";
            this.Order.Size = new System.Drawing.Size(120, 23);
            this.Order.TabIndex = 12;
            this.Order.Text = "label2";
            this.Order.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DeleteButton
            // 
            this.DeleteButton.BackColor = System.Drawing.Color.Salmon;
            this.DeleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.DeleteButton.Location = new System.Drawing.Point(9, 196);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(282, 26);
            this.DeleteButton.TabIndex = 11;
            this.DeleteButton.Text = "Delete this user";
            this.DeleteButton.UseVisualStyleBackColor = false;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // NextButton
            // 
            this.NextButton.Location = new System.Drawing.Point(216, 138);
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(75, 23);
            this.NextButton.TabIndex = 9;
            this.NextButton.Text = "Next";
            this.NextButton.UseVisualStyleBackColor = true;
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // PrevButton
            // 
            this.PrevButton.Location = new System.Drawing.Point(9, 138);
            this.PrevButton.Name = "PrevButton";
            this.PrevButton.Size = new System.Drawing.Size(75, 23);
            this.PrevButton.TabIndex = 8;
            this.PrevButton.Text = "Previous";
            this.PrevButton.UseVisualStyleBackColor = true;
            this.PrevButton.Click += new System.EventHandler(this.PrevButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(90, 138);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(120, 23);
            this.SaveButton.TabIndex = 7;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // PasswordRestrictionCheckBox
            // 
            this.PasswordRestrictionCheckBox.Location = new System.Drawing.Point(90, 108);
            this.PasswordRestrictionCheckBox.Name = "PasswordRestrictionCheckBox";
            this.PasswordRestrictionCheckBox.Size = new System.Drawing.Size(120, 24);
            this.PasswordRestrictionCheckBox.TabIndex = 6;
            this.PasswordRestrictionCheckBox.Text = "Password resriction";
            this.PasswordRestrictionCheckBox.UseVisualStyleBackColor = true;
            // 
            // IsBlockedCheckBox
            // 
            this.IsBlockedCheckBox.Location = new System.Drawing.Point(90, 78);
            this.IsBlockedCheckBox.Name = "IsBlockedCheckBox";
            this.IsBlockedCheckBox.Size = new System.Drawing.Size(120, 24);
            this.IsBlockedCheckBox.TabIndex = 5;
            this.IsBlockedCheckBox.Text = "Blocked";
            this.IsBlockedCheckBox.UseVisualStyleBackColor = true;
            // 
            // LoginOutput
            // 
            this.LoginOutput.Location = new System.Drawing.Point(90, 52);
            this.LoginOutput.Name = "LoginOutput";
            this.LoginOutput.ReadOnly = true;
            this.LoginOutput.Size = new System.Drawing.Size(120, 20);
            this.LoginOutput.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(19, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Login";
            // 
            // EndButton
            // 
            this.EndButton.Location = new System.Drawing.Point(216, 167);
            this.EndButton.Name = "EndButton";
            this.EndButton.Size = new System.Drawing.Size(75, 23);
            this.EndButton.TabIndex = 2;
            this.EndButton.Text = "To the end";
            this.EndButton.UseVisualStyleBackColor = true;
            this.EndButton.Click += new System.EventHandler(this.EndButton_Click);
            // 
            // BeginButton
            // 
            this.BeginButton.Location = new System.Drawing.Point(9, 167);
            this.BeginButton.Name = "BeginButton";
            this.BeginButton.Size = new System.Drawing.Size(75, 23);
            this.BeginButton.TabIndex = 1;
            this.BeginButton.Text = "To the begin";
            this.BeginButton.UseVisualStyleBackColor = true;
            this.BeginButton.Click += new System.EventHandler(this.BeginButton_Click);
            // 
            // AddUserButton
            // 
            this.AddUserButton.BackColor = System.Drawing.Color.LightGreen;
            this.AddUserButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.AddUserButton.Location = new System.Drawing.Point(9, 3);
            this.AddUserButton.Name = "AddUserButton";
            this.AddUserButton.Size = new System.Drawing.Size(282, 23);
            this.AddUserButton.TabIndex = 0;
            this.AddUserButton.Text = "Add new user";
            this.AddUserButton.UseVisualStyleBackColor = false;
            this.AddUserButton.Click += new System.EventHandler(this.addUserToolStripMenuItem_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(-3, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(299, 2);
            this.label3.TabIndex = 5;
            this.label3.Text = "label3";
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 394);
            this.Controls.Add(this.AdminPanel);
            this.Controls.Add(this.UserPanel);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.ChangePasswordButton);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Client";
            this.Tag = "";
            this.Text = "Client";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.AdminPanel.ResumeLayout(false);
            this.AdminPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.Label label3;

        private System.Windows.Forms.Label label2;

        private System.Windows.Forms.Label Order;

        private System.Windows.Forms.Button DeleteButton;

        private System.Windows.Forms.Button SaveButton;

        private System.Windows.Forms.Button PrevButton;

        private System.Windows.Forms.Button EndButton;

        private System.Windows.Forms.Button NextButton;
        private System.Windows.Forms.Button BeginButton;

        private System.Windows.Forms.CheckBox PasswordRestrictionCheckBox;
        private System.Windows.Forms.CheckBox IsBlockedCheckBox;

        private System.Windows.Forms.Button button5;

        private System.Windows.Forms.TextBox LoginOutput;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;

        private System.Windows.Forms.Button button2;

        private System.Windows.Forms.Button AddUserButton;

        private System.Windows.Forms.Button button1;

        private System.Windows.Forms.ToolStripMenuItem addUserToolStripMenuItem;

        private System.Windows.Forms.Panel UserPanel;
        private System.Windows.Forms.Panel AdminPanel;

        private System.Windows.Forms.Panel panel1;

        private System.Windows.Forms.Button ExitButton;

        private System.Windows.Forms.Button ChangePasswordButton;

        private System.Windows.Forms.Button ChangePassword;

        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem programToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;

        #endregion
    }
}