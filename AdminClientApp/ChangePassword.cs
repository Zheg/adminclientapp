﻿using System;
using System.Windows.Forms;

namespace AdminClientApp
{
    public partial class ChangePassword : Form
    {
        private readonly UserWithoutPassword _user;
        private readonly UserRepository _userRepository;
        private readonly Client _parent;

        public ChangePassword(UserWithoutPassword user, UserRepository userRepository, Client parent)
        {
            _user = user;
            _userRepository = userRepository;
            _parent = parent;
            InitializeComponent();
            if (_user.HasPasswordRestriction)
                MessageBox.Show("Password restriction:\n" +
                                "Passwords must contain at least one digit and at least one arithmetic operation(+, -, *, /).",
                    "Password change", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ChangePasswordButton_Click(object sender, EventArgs e)
        {
            if (!_userRepository.Auth(_user.Name, OldPasswordInput.Text))
            {
                MessageBox.Show("Wrong old password.", "Password change error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                OldPasswordInput.Text = "";
                return;
            }

            if (NewPasswordInput.Text != ConfirmPasswordInput.Text)
            {
                MessageBox.Show("Passwords do not match.", "Password change error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            if (NewPasswordInput.Text == "")
            {
                MessageBox.Show("Passwords can not be empty.", "Password change error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            if (_user.HasPasswordRestriction && !PasswordManager.HasRestrictions(NewPasswordInput.Text))
            {
                MessageBox.Show(
                    "Passwords must contain at least one digit and at least one arithmetic operation(+, -, *, /).",
                    "Password change error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var user = _user.AddPassword(NewPasswordInput.Text);

            _userRepository.Update(user);
            _parent.PasswordChanged = true;
            MessageBox.Show(
                "Passwords changed successfully",
                "Password change", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Close();
        }

        private void CancelPasswordChange_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}