﻿using System;
using System.Windows.Forms;

namespace AdminClientApp
{
    public partial class AddUser : Form
    {
        private readonly UserRepository _userRepository;

        public AddUser(UserRepository userRepository)
        {
            _userRepository = userRepository;
            InitializeComponent();
        }

        private void AddUserButton_Click(object sender, EventArgs e)
        {
            var userName = LoginInput.Text;
            if (_userRepository.HasUser(userName))
            {
                MessageBox.Show("User with that name already exists.", "Add user error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            _userRepository.Create(new User(
                userName,
                "",
                false,
                true));

            MessageBox.Show("User \"" + userName + "\" added.", "Add user", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}