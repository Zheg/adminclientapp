﻿using System;
using System.Windows.Forms;

namespace AdminClientApp
{
    public partial class InputPassphrase : Form
    {
        private LogIn _parent;
        public InputPassphrase(LogIn parent)
        {
            InitializeComponent();
            _parent = parent;
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            _parent.ProvidePassphrase(textBox1.Text);
            Close();
        }
    }
}